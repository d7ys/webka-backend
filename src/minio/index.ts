import configuration from '@/configuration'
import * as Minio from 'minio'

const minioClient = new Minio.Client({
  endPoint: configuration.MINIO_HOST,
  port: Number(configuration.MINIO_PORT),
  useSSL: false,
  accessKey: configuration.MINIO_ACCESS,
  secretKey: configuration.MINIO_SECRET,
})

export default minioClient
