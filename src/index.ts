import express from 'express'
import cors from 'cors'

import configuration from '@/configuration'
import router from '@/router'
import database from './database'

const app = express()

app.disable('x-powered-by')

app.use(
  cors({
    origin: 'https://webka.defaul7.net',
  }),
)
app.use(express.json())

app.use(function (req, res, next) {
  //Enabling CORS
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization',
  )
  next()
})

app.use(router)

database.initialize(() => {
  app.listen(configuration.PORT, () => {
    console.log(`[server]: server listen on port: ${configuration.PORT}`)
  })
})
