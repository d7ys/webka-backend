import * as Yup from 'yup'

const recommendationResponseSchema = Yup.array(
  Yup.object({
    id: Yup.string(),
    avatar: Yup.string().nullable(),
    username: Yup.string(),
    firstName: Yup.string(),
    lastName: Yup.string(),
    middleName: Yup.string(),
    birthday: Yup.date().nullable(),
    gender: Yup.string(),
  }),
)

export { recommendationResponseSchema }
