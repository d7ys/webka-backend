import dotenv from 'dotenv'

dotenv.config()

export default {
  PORT: process.env.PORT || 8000,
  ATLAS_URI: process.env.ATLAS_URI || '',
  JWT_SECRET: process.env.JWT_SECRET || '',

  MINIO_HOST: process.env.MINIO_HOST || '',
  MINIO_PORT: process.env.MINIO_PORT || '',
  MINIO_SECRET: process.env.MINIO_SECRET || '',
  MINIO_ACCESS: process.env.MINIO_ACCESS || '',
  MINIO_BUCKET: process.env.MINIO_BUCKET || '',
  MINIO_ENDPOINT_URL: process.env.MINIO_ENDPOINT_URL || '',
}
